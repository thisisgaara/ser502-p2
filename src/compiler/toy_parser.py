import re

#from regex_example import tokenizer
#from regex_example import cyd_regex
#import regex_example

class Scanner(object):
    def __init__(self):
        super(Scanner, self).__init__()
        self._tokens = []
        # Build regular expression for tokenizing
        self._token_regex = "[a-zA-Z][a-zA-Z0-9_]*"     # Identifier
        self._token_regex += "|(([1-9][0-9]*\.?[0-9]*)|(\.[0-9]+))([Ee][+-]?[0-9]+)?" #Float including Scientific notation #Frankly this is for a dummy commit
        self._token_regex += "|[0-9]+"                  # Integer
        self._token_regex += "|" + re.escape("==")
        self._token_regex += "|" + re.escape("!=")
        self._token_regex += "|" + re.escape(">=")
        self._token_regex += "|" + re.escape("<=")
        self._token_regex += "|[" + re.escape("=+-*/\\><!.,") + "]"
        self._token_regex += "|[" + re.escape("(){}") + "]"
        self._token_regex += "|[\S]"    # Any non-whitespace character
        self._token_regex = re.compile(self._token_regex)   #compile into regex object
        
    def scan(self):
        if(not self._tokens):     #check for empty string
            return None
        next_token = self._tokens[0]
        self._tokens = self._tokens[1:] #remove first token in list
        return next_token
        
    def tokenize(self, string):
        self._tokens = self._token_regex.findall(string)
#        print self._tokens
        
    def replace(self, token):
        self._tokens = [token] + self._tokens
        
    def empty(self):
        if(not self._tokens):     # Check if token list is empty
            return True
        return False



#Recursive Descent Parser
def ToyParser(prog):
    sc = Scanner()
    for line in prog:
        sc.tokenize(line)
        if(program_line(sc) == False):
            print "in the following line:\n\t" + "\"" + line + "\""
            return False
    return True
            


def program_line(sc):
    if(sc.empty()):
        return True
    return declaration(sc)
        


def declaration(sc):
    if(primative(sc) == False):
        return False
    if(identifier(sc) == False):
        return False
    if(not sc.empty()):
        if(sc.scan() == "="):
            print "=",
        if(value(sc) == False):
            return False
        if(not sc.empty()):
            return False
    print ""
    return True
        

def primative(sc):
    token = sc.scan()
    if(token == "int" or token == "float"):
        print token,
        return True
    print "  *** Unexpected token: \'" + token + "\'",
    return False

def identifier(sc):
    token = sc.scan()
    found = re.match("[a-zA-Z][a-zA-Z0-9_]*", token)
    if(found):
        print token,
        return True
    print "  *** Unexpected token: \'" + token + "\'",
    return False

def value(sc):
    token = sc.scan()
    found = re.match("[0-9]*\.[0-9]+|[0-9]+", token)
    if(found):
        print token,
        return True
    print "  *** Unexpected token: \'" + token + "\'",
    return False





#Set file name
file_name = "program.txt"

# Open and read program file    
program_file = open(file_name, 'r')
prog = program_file.read()

prog = prog.split('\n')
print ("\nDisplaying Program:")
for x in prog:
    print x


# Test Parser
print "\n\nParser Output:"
if(not ToyParser(prog)):
    print "\n   Parse error"