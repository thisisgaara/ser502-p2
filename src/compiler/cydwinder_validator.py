import re

from cydwinder_scanner import Scanner

#Recursive Descent Parser
def cydwinder_parser(prog):
    sc = Scanner(prog)
    if(not program(sc)):
        print "\n   Parse error at line: " + str(sc.get_line_number())
        return False
    return True
            

def program(sc):
    while(not sc.end()):
        if loop(sc):
            sc.next_line()
        elif function(sc):
            sc.next_line()
        elif program_line(sc):
            sc.next_line()
        else:
            return False
    return True
    

def program_line(sc):
    #TODO: add assignments
    return declaration(sc)


def function(sc):
    # keyword def
    if(sc.current() != "def"):
        return False
    sc.scan()
    # check for return type
    returnType = sc.current()
    if(not primative(sc)):
        sc.disp_token_error("return type")
        return False
    sc.scan()
    # TODO: store the function name and args in symbol table
    if not args(sc):
        return False
    sc.next_line()
    # update code to check the return type
    if(not block(sc)):
        return False
    return True

def args(sc):
    if(sc.current() != "("):
        return False
    while(sc.current() != ")"):
        sc.scan()
        if sc.current() == ")":
            sc.scan()
            return True
        elif not primative(sc) :
            return False
        else:
            if not identifier(sc):
                return False
            if sc.current() == ",":
                sc.current()
    if(not end_check(sc)):
        return False
    return True


def loop(sc):
    # keyword while
    if(sc.current() != "while"):
        return False
    # start of boolean check
    if(sc.scan() != "("):
        return False
    # check for variable identifier
    sc.scan()
    #TODO: change from identifier to logic result
    if(not identifier(sc)):
        return False
    if(sc.current() != ")"):
        return False
    if(not end_check(sc)):
        return False
    sc.next_line()
    if(not block(sc)):
        return False
    return True


def block(sc):
    token = sc.current()
    if(token != "{"):
        return False
    if(not end_check(sc)):
        return False
    sc.next_line()
    while(sc.current() != "}" ):
        if(not program_line(sc)):
            return False
        if(sc.next_line() == None):
            return False
    if(not end_check(sc)):
        return False
    return True


def declaration(sc):
    if(primative(sc) == False):
        return False
    if(identifier(sc) == False):
        return False
    # Check if declaration includes assignment
    if(not sc.empty()):
        if(sc.current() == "="):
            sc.scan()
            # Check if assigning a boolean value
            # TODO: update this to take <logic>
            if boolean(sc):
                return True
            # Check if assigning a mathematical expression
            (check, value) = expr(sc)
            if check:
                print "\n\n    Variable value = " + str(value)
                return True
            return False
    return True
        


def expr(sc):
    #print " #entering expr"
    (check, value1) = term(sc)
    if not check:
        return (False, None)
    if not sc.empty():
        token = sc.current()
        if(token == "+"):
            sc.scan()
            (check, value2) = expr(sc)
            if check:
                result = value1 + value2
                return (True, result)
            return (False, None)
        if(token == "-"):
            sc.scan()
            (check, value2) = expr(sc)
            if check:
                result = value1 - value2
                return (True, result)
            return (False, None)
    return (True, value1) 

        

def term(sc):
    #print " #entering term"
    (check, value1) = factor(sc)
    if not check:
        return (False, None)
    if not sc.empty():
        token = sc.current()
        if(token == "*"):
            sc.scan()
            (check, value2) = factor(sc)
            if check:
                result = value1 * value2
                return (True, result)
            return (False, None)
        if(token == "/"):
            sc.scan()
            (check, value2) = factor(sc)
            if check:
                result = value1 / value2
                return (True, result)
            return (False, None)
    return (True, value1)
            
    
def factor(sc):
    #print " #entering factor"
    token = sc.current()
    if token == "(":
        sc.scan()
        (check, value) = expr(sc)
        if(check and sc.current() == ")"):
            sc.scan()
            return (True, value)
        return (False, None)
    (check, token) = number(sc)
    if check:
        return (True, int(token))
    if identifier(sc):
        return (True, None)
    return (False, None)
  
  
def logic(sc):
	#TODO:implement
	check = identifier(sc)
	logical_op = ["<", "<=", ">", ">=", "==", "!="]
	if not check:
		return False
	token = sc.current()
	if token not in logical_op:
		return False
	sc.scan()
	check = identifier(sc)
	if not check:
		return False
	sc.scan()
	return True
	
	
'''
============== Atomic Types =================
'''


def primative(sc):
    token = sc.current()
    if(token == "int" or token == "boolean"):
        sc.scan()
        return True
    sc.disp_token_error("primative")
    return False


def identifier(sc):
    token = sc.current()
    found = re.match("[a-zA-Z][a-zA-Z0-9_]*", token)
    if(found):
        sc.scan()
        return True
    #sc.disp_token_error("identifier")
    return False


def boolean(sc):
    token = sc.current()
    if(token == "True" or token == "False"):
        sc.scan()
        return True
    return False


def number(sc):
    token = sc.current()
    found = re.match("[0-9]+", token)
    if(found):
        sc.scan()
        return (True, token)
    return False


"""
Prints current token to program echo and returns True if the end of a line is reached
Otherwise throws an error and returns False
"""
def end_check(sc):
    print sc.current(),
    if(not sc.empty()):
        sc.disp_token_error("end of this line")
        return False
    return True
    



#Set file name
file_name = "program.txt"

# Open and read program file    
program_file = open(file_name, 'r')
prog = program_file.read()

prog = prog.replace('\r', '')   # Removes '\r', making it Mac/Linux compatible
prog = prog.split('\n') # List of lines in program file
print ("\nDisplaying Program:")
for x in prog:
    print x


# Test Parser
print "\n\n========================================"
print "\nParser Output:"
cydwinder_parser(prog)
    
